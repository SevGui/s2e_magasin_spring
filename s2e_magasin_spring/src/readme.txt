Nom du projet : magasinier-spring
Techno : spring/hibernate

Pour qu'il fonctionne chez vous, il faut mettre vos données personnelles dans :
- la classe com.greta.s2e.magasinier.globals.ConstantesLocales
- le fichier de hibernate : WedContent/WEB-INF/hibernate.properties
- le fichier SpringDispatcher-servlet.xml :
	ligne 14 : mettre votre package de base 
	<context:component-scan base-package="com.greta.s2e.magasinier" />
	ligne 28 : mettre votre package de beans 
	<property name="packagesToScan" value="com.greta.s2e.magasinier.beans" />


Bibliothèques à mettre dans /WEB-INF/lib/ :
antlr-2.7.7.jar
aopalliance-1.0.jar
asm-5.0.4.jar
asm-attrs-2.2.3.jar
c3p0-0.9.2.1.jar
cglib-3.1.jar
commons-collections-3.2.1.jar
commons-logging-1.2.jar
dom4j-1.6.1.jar
ehcache-2.10.1.jar
geronimo-jta_1.1_spec-1.1.1.jar
hibernate-commons-annotations-5.0.0.Final.jar
hibernate-core-5.0.2.Final.jar
hibernate-entitymanager-5.0.2.Final.jar
hibernate-jpa-2.1-api-1.0.0.Final.jar
hibernate-validator-5.2.2.Final.jar
jandex-1.2.2.Final.jar
javassist-3.18.1-GA.jar
jboss-logging-3.3.0.Final.jar
jstl-1.2.jar
jta-1.1.jar
junit-4.12.jar
log4j-1.2.17.jar
ojdbc7.jar
persistence-api-1.0.2.jar
s2e-globals.jar
spring-aop-4.2.1.RELEASE.jar
spring-aspects-4.2.1.RELEASE.jar
spring-beans-4.2.1.RELEASE.jar
spring-context-4.2.1.RELEASE.jar
spring-context-support-4.2.1.RELEASE.jar
spring-core-4.2.1.RELEASE.jar
spring-expression-4.2.1.RELEASE.jar
spring-instrument-4.2.1.RELEASE.jar
spring-instrument-tomcat-4.2.1.RELEASE.jar
spring-jdbc-4.2.1.RELEASE.jar
spring-jms-4.2.1.RELEASE.jar
spring-messaging-4.2.1.RELEASE.jar
spring-orm-4.2.1.RELEASE.jar
spring-oxm-4.2.1.RELEASE.jar
spring-test-4.2.1.RELEASE.jar
spring-tx-4.2.1.RELEASE.jar
spring-web-4.2.1.RELEASE.jar
spring-webmvc-4.2.1.RELEASE.jar
spring-webmvc-portlet-4.2.1.RELEASE.jar
spring-websocket-4.2.1.RELEASE.jar
xerces-2.4.0.jar
