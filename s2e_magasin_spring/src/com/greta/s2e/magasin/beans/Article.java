package com.greta.s2e.magasin.beans;

import static com.greta.s2e.magasin.globals.Constants.*;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Classe entity Article
 */
@Entity
@Table(name = TABLE_ARTICLE)
public class Article {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = TABLE_ARTICLE_ID)
	protected int id;
	@Column(name = TABLE_ARTICLE_LIBELLE)
	protected String libelle;
	@Column(name = TABLE_ARTICLE_MARQUE)
	protected String marque;
	@Column(name = TABLE_ARTICLE_MODELE)
	protected String modele;
	@Column(name = TABLE_ARTICLE_EQUIVALENCE)
	protected int equivalence;
	@Column(name = TABLE_ARTICLE_PRIXESTIME)
	protected float prix;
	@Column(name = TABLE_ARTICLE_QUANTITETOTALE)
	protected double quantiteTotale;
	@OneToOne (mappedBy = "article")
	protected Stock stock;
	@OneToMany(mappedBy = "article") 	
	List<Demande> demandes = new ArrayList<>();
	@OneToMany(mappedBy = "article") 	
	List<Catalogue> catalogues = new ArrayList<>();
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public String getMarque() {
		return marque;
	}

	public void setMarque(String marque) {
		this.marque = marque;
	}

	public String getModele() {
		return modele;
	}

	public void setModele(String modele) {
		this.modele = modele;
	}

	public int getEquivalence() {
		return equivalence;
	}

	public void setEquivalence(int equivalence) {
		this.equivalence = equivalence;
	}

	public float getPrix() {
		return prix;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}
	
	public void majPrix(float nouveauPrix, double nouvelleQuantite) {
		float quantiteTotale = (float) this.quantiteTotale;
		float newQuantite = (float) nouvelleQuantite;
		this.prix = ((this.prix * quantiteTotale) + (nouveauPrix * newQuantite))/(quantiteTotale + newQuantite);
	}

	public double getQuantiteTotale() {
		return quantiteTotale;
	}

	public void setQuantiteTotale(double quantiteTotale) {
		this.quantiteTotale = quantiteTotale;
	}
	
	public void addQuantite(double quantiteTotale) {
		this.quantiteTotale += quantiteTotale;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	
	public List<Demande> getDemandes() {
		return demandes;
	}

	public void setDemandes(List<Demande> demandes) {
		this.demandes = demandes;
	}

	public List<Catalogue> getCatalogues() {
		return catalogues;
	}

	public void setCatalogues(List<Catalogue> catalogues) {
		this.catalogues = catalogues;
	}

	public String getPrixFormate() {
		NumberFormat numberFormat = NumberFormat.getInstance(java.util.Locale.FRENCH);
		numberFormat.setMinimumFractionDigits(2);
		return numberFormat.format(prix);
	}

}
