package com.greta.s2e.magasin.beans;

import static com.greta.s2e.magasin.globals.Constants.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Classe entity Etablissement
 */
@Entity
@Table(name = TABLE_ETABLISSEMENT)
public class Etablissement {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = TABLE_ETABLISSEMENT_ID)
	protected int id;
	@Column(name = TABLE_ETABLISSEMENT_NOM)
	protected String nom;
	@Column(name = TABLE_ETABLISSEMENT_ADRESSE)
	protected String adresse;
	@Column(name = TABLE_ETABLISSEMENT_CODEPOSTAL)
	protected String codePostal;
	@Column(name = TABLE_ETABLISSEMENT_VILLE)
	protected String ville;
	@Column(name = TABLE_ETABLISSEMENT_BUDGET)
	protected float budget;
	@Column(name = TABLE_ETABLISSEMENT_BUDGET_REST)
	protected float budgetRest;
	@OneToMany(mappedBy = "etablissement")
	List<Utilisateur> utilisateurs = new ArrayList<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public float getBudget() {
		return budget;
	}

	public void setBudget(float budget) {
		this.budget = budget;
	}

	/**
	 * @return the budgetRest
	 */
	public float getBudgetRest() {
		return budgetRest;
	}

	/**
	 * @param budgetRest the budgetRest to set
	 */
	public void setBudgetRest(float budgetRest) {
		this.budgetRest = budgetRest;
	}

	public List<Utilisateur> getUtilisateurs() {
		return utilisateurs;
	}

	public void setUtilisateurs(List<Utilisateur> utilisateurs) {
		this.utilisateurs = utilisateurs;
	}

	@Override
	public boolean equals(Object obj) {
		Etablissement etablissement = (Etablissement) obj;
		if (null == etablissement) return false;
		return (id == etablissement.getId());
	}

}
