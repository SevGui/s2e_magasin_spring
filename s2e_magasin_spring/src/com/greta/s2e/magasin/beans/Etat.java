package com.greta.s2e.magasin.beans;

import static com.greta.s2e.magasin.globals.Constants.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Classe entity Etat
 */
@Entity
@Table(name = TABLE_ETAT)
public class Etat {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = TABLE_ETAT_ID)
	protected String id;
	@Column(name = TABLE_ETAT_LIBELLE)
	protected String libelle;
	@OneToMany(mappedBy = "etat") 	
	List<Demande> demandes = new ArrayList<>();
	@OneToMany(mappedBy = "etat") 	
	List<Commande> commandes = new ArrayList<>();

//	public static final String ATTENTE_VALIDATION	= "AV";
//	public static final String REFUSE				= "RF";
//	public static final String EN_COURS				= "EC";
//	public static final String ATTENTE_PRODUIT		= "AP";
//	public static final String LIVRE				= "LV";

	
public Etat() {}
	
	public Etat(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<Demande> getDemandes() {
		return demandes;
	}

	public void setDemandes(List<Demande> demandes) {
		this.demandes = demandes;
	}

	public List<Commande> getCommandes() {
		return commandes;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}

}
