package com.greta.s2e.magasin.beans;

import static com.greta.s2e.magasin.globals.Constants.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Classe entity Utilisateur
 */
@Entity
@Table(name = TABLE_UTILISATEUR)
public class Utilisateur {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = TABLE_UTILISATEUR_ID)
	protected int id;
	@ManyToOne
	@JoinColumn (name = TABLE_UTILISATEUR_IDROLE)
	protected Role role;
	@Column(name = TABLE_UTILISATEUR_NOM)
	protected String nom;
	@Column(name = TABLE_UTILISATEUR_PRENOM)
	protected String prenom;
	@ManyToOne
	@JoinColumn (name = TABLE_UTILISATEUR_IDETABLISSEMENT)
	protected Etablissement etablissement;
	@Column(name = TABLE_UTILISATEUR_LOGIN)
	protected String login;
	@Column(name = TABLE_UTILISATEUR_PASSWORD)
	protected String password;
	@OneToMany(mappedBy = "demandeur") 	
	List<Demande> demandes = new ArrayList<>();
	@OneToMany(mappedBy = "commanditaire") 	
	List<Commande> commandes = new ArrayList<>();

	public Utilisateur() {}

	public Utilisateur(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Etablissement getEtablissement() {
		return etablissement;
	}

	public void setEtablissement(Etablissement etablissement) {
		this.etablissement = etablissement;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Demande> getDemandes() {
		return demandes;
	}

	public void setDemandes(List<Demande> demandes) {
		this.demandes = demandes;
	}

	public List<Commande> getCommandes() {
		return commandes;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}

}
