package com.greta.s2e.magasin.beans;

import static com.greta.s2e.magasin.globals.Constants.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Classe entity Stock
 */
@Entity
@Table(name = TABLE_STOCK)
public class Stock {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = TABLE_STOCK_ID)
	protected int id;
	@Column(name = TABLE_STOCK_QUANTITE)
	protected int quantite;
	@Column(name = TABLE_STOCK_SEUILMIN)
	protected int seuilMinimum;
	@Column(name = TABLE_STOCK_SEUILMAX)
	protected int seuilMaximum;
	@OneToOne
	@JoinColumn (name = TABLE_STOCK_IDARTICLE)
	protected Article article;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	
	public void addQuantite(int quantite) {
		this.quantite += quantite;
	}
	
	public void delQuantite(int quantite) {
		this.quantite -= quantite;
	}

	public int getSeuilMinimum() {
		return seuilMinimum;
	}

	public void setSeuilMinimum(int seuilMinimum) {
		this.seuilMinimum = seuilMinimum;
	}

	public int getSeuilMaximum() {
		return seuilMaximum;
	}

	public void setSeuilMaximum(int seuilMaximum) {
		this.seuilMaximum = seuilMaximum;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

}
