package com.greta.s2e.magasin.beans;

import static com.greta.s2e.magasin.globals.Constants.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Classe entity Catalogue
 */
@Entity
@Table(name = TABLE_CATALOGUE)
public class Catalogue {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = TABLE_CATALOGUE_ID)
	protected int id;
	@ManyToOne
	@JoinColumn (name = TABLE_CATALOGUE_IDFOURNISSEUR)
	protected Fournisseur fournisseur;
	@ManyToOne
	@JoinColumn (name = TABLE_CATALOGUE_IDARTICLE)
	protected Article article;
	@Column(name = TABLE_CATALOGUE_PRIX)
	protected float prix;
	@Column(name = TABLE_CATALOGUE_DELAI)
	protected int delai;
	@OneToMany(mappedBy = "articleCatalogue")
	List<Commande> commandes = new ArrayList<>();

	
public Catalogue() {}
	
	public Catalogue(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Fournisseur getFournisseur() {
		return fournisseur;
	}

	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public float getPrix() {
		return prix;
	}

	public void setPrix(float prix) {
		this.prix = prix;
	}

	public int getDelai() {
		return delai;
	}

	public void setDelai(int delai) {
		this.delai = delai;
	}

	public List<Commande> getCommandes() {
		return commandes;
	}

	public void setCommandes(List<Commande> commandes) {
		this.commandes = commandes;
	}

}
