package com.greta.s2e.magasin.beans;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import static com.greta.s2e.magasin.globals.Constants.*;

/**
 * Classe entity Demande
 */
@Entity
@Table(name = TABLE_DEMANDE)
public class Demande {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = TABLE_DEMANDE_ID)
	protected int id;
	@Column(name = TABLE_DEMANDE_DATE)
	protected Date date;
	@Column(name = TABLE_DEMANDE_DELAI)
	protected int delai;
	@Column(name = TABLE_DEMANDE_QUANTITE)
	protected int quantite;
	@ManyToOne
	@JoinColumn (name = TABLE_DEMANDE_IDARTICLE)
	protected Article article;
	@ManyToOne
	@JoinColumn (name = TABLE_DEMANDE_IDUTILISATEUR)
	protected Utilisateur demandeur;
	@ManyToOne
	@JoinColumn (name = TABLE_DEMANDE_IDETAT)
	protected Etat etat;

	
	/**
	 * 
	 */
	public Demande() {
		super();
	}

	
	/**
	 * @param id
	 * @param etat
	 */
	public Demande(int id, Etat etat) {
		super();
		this.id = id;
		this.etat = etat;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getDelai() {
		return delai;
	}

	public void setDelai(int delai) {
		this.delai = delai;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public Article getArticle() {
		return article;
	}

	public void setArticle(Article article) {
		this.article = article;
	}

	public Utilisateur getDemandeur() {
		return demandeur;
	}

	public void setDemandeur(Utilisateur demandeur) {
		this.demandeur = demandeur;
	}

	public Etat getEtat() {
		return etat;
	}

	public void setEtat(Etat etat) {
		this.etat = etat;
	}

}
