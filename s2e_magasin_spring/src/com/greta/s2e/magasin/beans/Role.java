package com.greta.s2e.magasin.beans;

import static com.greta.s2e.magasin.globals.Constants.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Classe entity Role
 */
@Entity
@Table(name = TABLE_ROLE)
public class Role {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = TABLE_ROLE_ID)
	protected String id;
	@Column(name = TABLE_ROLE_LIBELLE)
	protected String libelle;
	@OneToMany(mappedBy = "role") 	
	List<Utilisateur> utilisateurs = new ArrayList<>();

//	public static final String ADMINISTRATEUR	= "AM";
//	public static final String ETABLISSEMENT	= "ET";
//	public static final String MAGASINIER		= "MG";
//	public static final String SIEGE			= "SG";

	public Role() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public List<Utilisateur> getUtilisateurs() {
		return utilisateurs;
	}

	public void setUtilisateurs(List<Utilisateur> utilisateurs) {
		this.utilisateurs = utilisateurs;
	}

	@Override
	public boolean equals(Object obj) {
		Role role = (Role) obj;

		if(null == role){
			return false;
		}
		if (null == id){
			return (null == role.getId());
		}
		return (id.equals(role.getId()));
	}
	
}
