package com.greta.s2e.magasin.beans;

import static com.greta.s2e.magasin.globals.Constants.*;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Classe entity Commande
 */
@Entity
@Table(name = TABLE_COMMANDE)
public class Commande {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = TABLE_COMMANDE_ID)
	protected int id;
	@Column(name = TABLE_COMMANDE_DATE)
	protected Date date;
	@Column(name = TABLE_COMMANDE_DELAI)
	protected int delai;
	@Column(name = TABLE_COMMANDE_QUANTITE)
	protected int quantite;
	@ManyToOne
	@JoinColumn(name = TABLE_COMMANDE_IDCATALOGUE)
	protected Catalogue articleCatalogue;
	@ManyToOne
	@JoinColumn(name = TABLE_COMMANDE_IDUTILISATEUR)
	protected Utilisateur commanditaire;
	@ManyToOne
	@JoinColumn(name = TABLE_COMMANDE_IDETAT)
	protected Etat etat;

	public Commande() {
	}

	public Commande(int quantite, Catalogue articleCatalogue) {
		this.quantite = quantite;
		this.articleCatalogue = articleCatalogue;
	}

	public Commande(Date date, int delai, int quantite, Catalogue articleCatalogue, Utilisateur commanditaire,
			Etat etat) {
		this.date = date;
		this.delai = delai;
		this.quantite = quantite;
		this.articleCatalogue = articleCatalogue;
		this.commanditaire = commanditaire;
		this.etat = etat;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getDelai() {
		return delai;
	}

	public void setDelai(int delai) {
		this.delai = delai;
	}

	public int getQuantite() {
		return quantite;
	}

	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

	public Catalogue getArticleCatalogue() {
		return articleCatalogue;
	}

	public void setArticleCatalogue(Catalogue articleCatalogue) {
		this.articleCatalogue = articleCatalogue;
	}

	public Utilisateur getCommanditaire() {
		return commanditaire;
	}

	public void setCommanditaire(Utilisateur commanditaire) {
		this.commanditaire = commanditaire;
	}

	public Etat getEtat() {
		return etat;
	}

	public void setEtat(Etat etat) {
		this.etat = etat;
	}

}
