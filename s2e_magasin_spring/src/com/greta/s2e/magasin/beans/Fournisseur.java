package com.greta.s2e.magasin.beans;

import static com.greta.s2e.magasin.globals.Constants.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Classe entity Fournisseur
 */
@Entity
@Table(name = TABLE_FOURNISSEUR)
public class Fournisseur {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = TABLE_FOURNISSEUR_ID)
	protected int id;
	@Column(name = TABLE_FOURNISSEUR_NOM)
	protected String nom;
	@Column(name = TABLE_FOURNISSEUR_ADRESSE)
	protected String adresse;
	@Column(name = TABLE_FOURNISSEUR_CODEPOSTAL)
	protected String codePostal;
	@Column(name = TABLE_FOURNISSEUR_VILLE)
	protected String ville;
	@OneToMany(mappedBy = "fournisseur")
	List<Catalogue> catalogues = new ArrayList<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getCodePostal() {
		return codePostal;
	}

	public void setCodePostal(String codePostal) {
		this.codePostal = codePostal;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public List<Catalogue> getCatalogues() {
		return catalogues;
	}

	public void setCatalogues(List<Catalogue> catalogues) {
		this.catalogues = catalogues;
	}

}
