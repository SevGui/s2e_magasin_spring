package com.greta.s2e.magasin.handlers;

import static com.greta.s2e.magasin.globals.Constants.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.greta.s2e.magasin.beans.Utilisateur;
import com.greta.s2e.magasin.services.UtilisateurService;

public class ConnexionHandlerInterceptor extends HandlerInterceptorAdapter {

	public static final Logger LOGGER = Logger.getLogger(ConnexionHandlerInterceptor.class);
	
	@Autowired
	private UtilisateurService service;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		HttpSession session = request.getSession();
		Cookie[] cookies = request.getCookies();
		
		Utilisateur utilisateur = null;
		
		//récupération de l'utilisateur à partir du cookie générique créé dans le site de connexion
		if (null != cookies){
			for (Cookie cookie : cookies){
				if (cookie.getName().equals(COOKIE_S2E)){
					LOGGER.info("Récupération du cookie générique : "+ cookie.getValue());
					utilisateur = getUtilisateurById(cookie.getValue());
					break;
				}
			}
			//destruction du cookie générique
			destructionCookie(response);
		}
		
		//si l'utilisateur est null, récupération de l'utilisateur dans la session
		if (null == utilisateur){
			utilisateur = (Utilisateur) session.getAttribute(SESSION_S2E);
		}
		
		//si l'utilisateur n'est pas retrouvé ou s'il n'a pas le droit d'être là 
		if(null == utilisateur || !utilisateur.getRole().getId().equals(DROIT_MAGASIN)){
			LOGGER.info("Utilisateur non reconnu ou non autorisé : redirection sur la page de connexion");
			//retour à la page de connexion
			response.sendRedirect(REDIRECTION_LOGIN);
			return false;
		} else {	//tout est ok
			LOGGER.info("Utilisateur connecté");
			//enregistrement de l'utilisateur en session
	        session.setAttribute( SESSION_S2E, utilisateur );
	        request.setAttribute("user", utilisateur);
	        //poursuite de la navigation
			return true;

		}
	}

	private Utilisateur getUtilisateurById(String id){
		if(id.equals(null) || id.isEmpty() || id.equals("")) return null;
		return service.getElementById(Integer.valueOf(id));
	}
	
	private void destructionCookie(HttpServletResponse response){
		Cookie cookie = new Cookie(COOKIE_S2E,"");
		cookie.setPath(COOKIE_S2E_PATH);
		cookie.setMaxAge(-1);
		response.addCookie(cookie);
		
		LOGGER.info("Destruction du cookie");
	}

}
