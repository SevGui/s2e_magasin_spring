package com.greta.s2e.magasin.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.greta.s2e.magasin.beans.Commande;

@Repository
public class CommandeDao extends Dao implements IDao<Commande> {

	private static final Logger LOGGER = Logger.getLogger(CommandeDao.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Commande> getList() {
		initSession();
		String hql = "FROM Commande";
		Query query = session.createQuery(hql);
		List<Commande> liste = query.list();
		LOGGER.info("Liste commande ::" + liste);
		session.close();
		return liste;
	}

	@Override
	public int create(Commande element) {
		int result = 0;
		initSession();
		Transaction transaction = session.beginTransaction();
		transaction.begin();
		result = (int) session.save(element);
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	@Override
	public int update(Commande element) {
		int result = 0;
		initSession();
		Transaction transaction = session.beginTransaction();
		session.update(element);
		transaction.commit();
		session.close();
		return element.getId();
	}

	@Override
	public int delete(Commande element) {
		initSession();
		Transaction transaction = session.beginTransaction();
		session.delete(element);
		transaction.commit();
		session.close();
		return element.getId();
	}

	@Override
	public Commande getElementById(int id) {
		initSession();
		Transaction transaction = session.beginTransaction();
		Commande element = (Commande) session.load(Commande.class, id);
		transaction.commit();
		//session.close();
		return element;
	}
	
}