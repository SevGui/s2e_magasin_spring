package com.greta.s2e.magasin.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.greta.s2e.magasin.beans.Demande;

@Repository
public class DemandeDao extends Dao implements IDao<Demande> {

	private static final Logger LOGGER = Logger.getLogger(DemandeDao.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Demande> getList() {
		initSession();
		String hql = "FROM Demande";
		Query query = session.createQuery(hql);
		List<Demande> liste = query.list();
		LOGGER.info("Liste demande ::" + liste);
		session.close();
		return liste;
	}

	@Override
	public int create(Demande element) {
		int result = 0;
		initSession();
		Transaction transaction = session.beginTransaction();
		transaction.begin();
		result = (int) session.save(element);
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	@Override
	public int update(Demande element) {
		int result = 0;
		initSession();
		Transaction transaction = session.beginTransaction();
		session.update(element);
		transaction.commit();
		session.close();
		return element.getId();
	}

	@Override
	public int delete(Demande element) {
		initSession();
		Transaction transaction = session.beginTransaction();
		session.delete(element);
		transaction.commit();
		session.close();
		return element.getId();
	}

	@Override
	public Demande getElementById(int id) {
		initSession();
		Transaction transaction = session.beginTransaction();
		Demande element = (Demande) session.load(Demande.class, id);
		transaction.commit();
		//session.close();
		return element;
	}
	
}