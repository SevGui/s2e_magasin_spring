package com.greta.s2e.magasin.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class Dao {
	
	@Autowired
	protected SessionFactory sessionFactory;
	protected Session session;
	
	protected void initSession(){
		if(session == null || !session.isOpen()){
			session = sessionFactory.openSession();
		}
	}

}
