package com.greta.s2e.magasin.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.greta.s2e.magasin.beans.Article;

@Repository
public class ArticleDao extends Dao implements IDao<Article> {

	private static final Logger LOGGER = Logger.getLogger(ArticleDao.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Article> getList() {
		initSession();
		String hql = "FROM Article";
		Query query = session.createQuery(hql);
		List<Article> liste = query.list();
		LOGGER.info("Liste article ::" + liste);
		session.close();
		return liste;
	}

	@Override
	public int create(Article element) {
		int result = 0;
		initSession();
		Transaction transaction = session.beginTransaction();
		transaction.begin();
		result = (int) session.save(element);
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	@Override
	public int update(Article element) {
		int result = 0;
		initSession();
		Transaction transaction = session.beginTransaction();
		session.update(element);
		transaction.commit();
		session.close();
		return element.getId();
	}

	@Override
	public int delete(Article element) {
		initSession();
		Transaction transaction = session.beginTransaction();
		session.delete(element);
		transaction.commit();
		session.close();
		return element.getId();
	}

	@Override
	public Article getElementById(int id) {
		initSession();
		Transaction transaction = session.beginTransaction();
		Article element = (Article) session.load(Article.class, id);
		transaction.commit();
		//session.close();
		return element;
	}
	
}