package com.greta.s2e.magasin.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.greta.s2e.magasin.beans.Catalogue;

@Repository
public class CatalogueDao extends Dao implements IDao<Catalogue>{

	private static final Logger LOGGER = Logger.getLogger(CatalogueDao.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Catalogue> getList() {
		initSession();
		String hql = "FROM Catalogue";
		Query query = session.createQuery(hql);
		List<Catalogue> liste = query.list();
		LOGGER.info("Liste catalogue ::" + liste);
		session.close();
		return liste;
	}

	@Override
	public int create(Catalogue element) {
		int result = 0;
		initSession();
		Transaction transaction = session.beginTransaction();
		transaction.begin();
		result = (int) session.save(element);
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	@Override
	public int update(Catalogue element) {
		int result = 0;
		initSession();
		Transaction transaction = session.beginTransaction();
		session.update(element);
		transaction.commit();
		session.close();
		return element.getId();
	}

	@Override
	public int delete(Catalogue element) {
		initSession();
		Transaction transaction = session.beginTransaction();
		session.delete(element);
		transaction.commit();
		session.close();
		return element.getId();
	}

	@Override
	public Catalogue getElementById(int id) {
		initSession();
		Transaction transaction = session.beginTransaction();
		Catalogue element = (Catalogue) session.load(Catalogue.class, id);
		transaction.commit();
		//session.close();
		return element;
	}

	@SuppressWarnings("unchecked")
	public List<Catalogue> getListByArticle(int idArticle) {
		initSession();
		String hql = "FROM Catalogue WHERE article.id="+ idArticle;
		Query query = session.createQuery(hql);
		List<Catalogue> liste = query.list();
		LOGGER.info("Liste catalogue par article ::" + liste);
		return liste;
	}

}
