package com.greta.s2e.magasin.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.greta.s2e.magasin.beans.Utilisateur;


@Repository
public class UtilisateurDao extends Dao implements IDao<Utilisateur> {

	private static final Logger LOGGER = Logger.getLogger(UtilisateurDao.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Utilisateur> getList() {
		initSession();
		String hql = "FROM Utilisateur";
		Query query = session.createQuery(hql);
		List<Utilisateur> liste = query.list();
		LOGGER.info("Liste utilisateurs ::" + liste);
		session.close();
		return liste;
	}

	@Override
	public int create(Utilisateur element) {
		int result = 0;
		initSession();
		Transaction transaction = session.beginTransaction();
		transaction.begin();
		result = (int) session.save(element);
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	@Override
	public int update(Utilisateur element) {
		int result = 0;
		initSession();
		Transaction transaction = session.beginTransaction();
		session.update(element);
		transaction.commit();
		session.close();
		return element.getId();
	}

	@Override
	public int delete(Utilisateur element) {
		initSession();
		Transaction transaction = session.beginTransaction();
		session.delete(element);
		transaction.commit();
		session.close();
		return element.getId();
	}

	@Override
	public Utilisateur getElementById(int id) {
		initSession();
		Transaction transaction = session.beginTransaction();
		Utilisateur element = (Utilisateur) session.load(Utilisateur.class, id);
		transaction.commit();
		//session.close();
		return element;
	}
	
}