package com.greta.s2e.magasin.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import com.greta.s2e.magasin.beans.Stock;

@Repository
public class StockDao extends Dao implements IDao<Stock> {

	private static final Logger LOGGER = Logger.getLogger(StockDao.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Stock> getList() {
		initSession();
		String hql = "FROM Stock";
		Query query = session.createQuery(hql);
		List<Stock> liste = query.list();
		LOGGER.info("Liste stock ::" + liste);
		session.close();
		return liste;
	}

	@Override
	public int create(Stock element) {
		int result = 0;
		initSession();
		Transaction transaction = session.beginTransaction();
		transaction.begin();
		result = (int) session.save(element);
		session.getTransaction().commit();
		session.close();
		return result;
	}
	
	@Override
	public int update(Stock element) {
		int result = 0;
		initSession();
		Transaction transaction = session.beginTransaction();
		session.update(element);
		transaction.commit();
		session.close();
		return element.getId();
	}

	@Override
	public int delete(Stock element) {
		initSession();
		Transaction transaction = session.beginTransaction();
		session.delete(element);
		transaction.commit();
		session.close();
		return element.getId();
	}

	@Override
	public Stock getElementById(int id) {
		initSession();
		Transaction transaction = session.beginTransaction();
		Stock element = (Stock) session.load(Stock.class, id);
		transaction.commit();
		//session.close();
		return element;
	}
	
}