package com.greta.s2e.magasin.exceptions;

public class CommandeException extends RuntimeException {

	private String exceptionMessage;
	
	public CommandeException() {}

	public CommandeException(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}

	public String getExceptionMessage() {
		return exceptionMessage;
	}

	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}
	
}
