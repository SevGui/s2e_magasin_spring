package com.greta.s2e.magasin.controllers;

import static com.greta.s2e.magasin.globals.Constants.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.greta.s2e.magasin.beans.Article;
import com.greta.s2e.magasin.beans.Commande;
import com.greta.s2e.magasin.beans.Demande;
import com.greta.s2e.magasin.beans.Etat;
import com.greta.s2e.magasin.services.ArticleService;
import com.greta.s2e.magasin.services.CatalogueService;
import com.greta.s2e.magasin.services.DemandeService;

@Controller
@RequestMapping(value = "/demandes")
public class DemandeController {
	public static final Logger LOGGER = Logger.getLogger(DemandeController.class);

	@Autowired
	private DemandeService serviceDemande;
	@Autowired
	private ArticleService serviceArticle;
	@Autowired
	private CatalogueService serviceCatalogue;

	// liste des demandes
	@RequestMapping(method = RequestMethod.GET)
	public String listDemandes(ModelMap model) {
		LOGGER.info("initialisation de la liste des demandes");
		model.addAttribute("liste", serviceDemande.getList());
		return "liste_demandes";
	}

	// formulaire de création d'une commande à partir d'une demande
	@RequestMapping(value = "/new/{idDmd}/{idArt}", method = RequestMethod.GET)
	public String newCommandeArticle(ModelMap model, @PathVariable int idDmd, @PathVariable int idArt) {
		LOGGER.info("initialisation du formulaire de creation d'une commande par une demande");
		LOGGER.info("id demande : " + idDmd);
		LOGGER.info("id article : " + idArt);

		Demande dmd = new Demande();
		dmd = serviceDemande.getElementById(idDmd);
		LOGGER.info("id article : " + dmd.getId());
		dmd.setEtat(new Etat(ATART_ETAT));
		LOGGER.info("etat article : " + dmd.getEtat().getLibelle());

		int res = serviceDemande.update(dmd);
		LOGGER.info("update ok : demande " + res);

		model.addAttribute("article", serviceArticle.getElementById(idArt));
		model.addAttribute("catalogues", serviceCatalogue.getListByArticle(idArt));
		return "new_commande";
	}

	// update d'une demande pour livraison
	@RequestMapping(value = "/livrer/{id}", method = RequestMethod.GET)
	public ModelAndView livraison(@PathVariable int id) {
		LOGGER.info("livraison d'une demande");
		LOGGER.info("id demande : " + id);
		Demande dmd = serviceDemande.getElementById(id);
		System.out.println("ok : " + dmd.getId());
		if (dmd != null) {

			Article article = dmd.getArticle();

			LOGGER.info("décrément du stock de l'article demandé");
			article.getStock().delQuantite(dmd.getQuantite());

			LOGGER.info("mise à  jour de la demande : état livré");
			dmd.setEtat(new Etat(LIVR_ETAT));
			int res = serviceDemande.update(dmd);

		}
		return new ModelAndView("redirect:/demandes");
	}

}
