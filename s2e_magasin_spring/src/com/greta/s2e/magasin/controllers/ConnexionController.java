package com.greta.s2e.magasin.controllers;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import static com.greta.s2e.magasin.globals.Constants.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/deconnexion")
public class ConnexionController {
	public static final Logger LOGGER = Logger.getLogger(ConnexionController.class);
	
	//deconnexion
	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView createCommande(HttpServletRequest request){
		//Destruction de la session
		LOGGER.info("Déconnexion : destruction de la session");
		HttpSession session = request.getSession();
		session.invalidate();
		//redirection sur la page de connexion
		return new ModelAndView("redirect:"+ REDIRECTION_LOGIN);
	}
}
