package com.greta.s2e.magasin.controllers;

import static com.greta.s2e.magasin.globals.Constants.*;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.greta.s2e.magasin.beans.Article;
import com.greta.s2e.magasin.beans.Catalogue;
import com.greta.s2e.magasin.beans.Commande;
import com.greta.s2e.magasin.beans.Etat;
import com.greta.s2e.magasin.beans.Utilisateur;
import com.greta.s2e.magasin.exceptions.CommandeException;
import com.greta.s2e.magasin.services.ArticleService;
import com.greta.s2e.magasin.services.CatalogueService;
import com.greta.s2e.magasin.services.CommandeService;
import com.greta.s2e.magasin.services.StockService;

@Controller
@RequestMapping(value = "/commandes")
public class CommandeController {
	public static final Logger LOGGER = Logger.getLogger(CommandeController.class);
	private int idUtilisateur = 3;
	
	@Autowired
	private CommandeService serviceCommande;
	@Autowired
	private ArticleService serviceArticle;
	@Autowired
	private CatalogueService serviceCatalogue;
	@Autowired
	private StockService serviceStock;
	
	//liste des commandes
	@RequestMapping(method = RequestMethod.GET)
	public String listCommandes(ModelMap model) {
		LOGGER.info("initialisation de la liste des commandes");
		model.addAttribute("liste", serviceCommande.getList());
		return "liste_commandes";
	}
	
	
	//formulaire de création d'une commande
		@RequestMapping(value = "/new/{id}", method = RequestMethod.GET)
		public String newCommandeArticle(ModelMap model, @PathVariable int id) {
			LOGGER.info("initialisation du formulaire de creation");
			LOGGER.info("id article : "+ id);
			model.addAttribute("article", serviceArticle.getElementById(id));
			model.addAttribute("catalogues" ,serviceCatalogue.getListByArticle(id));
			return "new_commande";
		}
	
	//traitement du formulaire de création d'une commande
		@RequestMapping(value="/enregistrer", method = RequestMethod.POST)
		@ExceptionHandler({ CommandeException.class })
		public ModelAndView createCommande(HttpServletRequest request, ModelMap model){
			LOGGER.info("création d'une commande");
			
			int idCatalogue;
			if (!verifInteger(request.getParameter(PARAM_CATALOGUE))) {
				throw new CommandeException("Fournisseur non valide");
			} else {
				idCatalogue = Integer.parseInt(request.getParameter(PARAM_CATALOGUE));
				if (idCatalogue<1){
					throw new CommandeException("Fournisseur non valide");
				}
			}

			int quantite;
			if (!verifInteger(request.getParameter(PARAM_QUANTITE))) {
				throw new CommandeException("Quantité non valide");
			} else {
				quantite = Integer.parseInt(request.getParameter(PARAM_QUANTITE));
				if (quantite<1){
					throw new CommandeException("Quantité non valide");
				}
			}

			java.util.Date date = new java.util.Date();
			int res = serviceCommande.create(new Commande(new java.sql.Date(date.getTime()), 0, quantite, new Catalogue(idCatalogue), new Utilisateur(idUtilisateur), new Etat(VALI_ETAT)));
			LOGGER.info("création ok : commande " + res);		
			
			return new ModelAndView("redirect:/commandes");
		}
		
		//update d'une commande qui a été livré
		@RequestMapping(value = "/livrer/{id}", method = RequestMethod.GET)
		public ModelAndView livraison(@PathVariable int id) {
			LOGGER.info("livraison d'une commande");
			LOGGER.info("id commande : "+ id);
			Commande commande = serviceCommande.getElementById(id);
			System.out.println("ok : " + commande.getId());
			if (commande != null){
				
				LOGGER.info("mise à  jour de la quantité totale et du prix moyen de l'article commande");
				Article article = commande.getArticleCatalogue().getArticle();
				article.majPrix(commande.getArticleCatalogue().getPrix(), commande.getQuantite());
				article.addQuantite(commande.getQuantite());
				
				LOGGER.info("incrément du stock de l'article commandé");
				article.getStock().addQuantite(commande.getQuantite());
				
				LOGGER.info("mise à  jour de la commande : état livré");
				commande.setEtat(new Etat(LIVR_ETAT));
				int res = serviceCommande.update(commande);
				
			}
			return new ModelAndView("redirect:/commandes");
		}
		
		private boolean verifInteger(String texte){
			Pattern p = Pattern.compile("^[0-9]+$");
			Matcher m = p.matcher(texte);
			return m.matches();
		}

}
