package com.greta.s2e.magasin.controllers;

import static com.greta.s2e.magasin.globals.Constants.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.greta.s2e.magasin.beans.Article;
import com.greta.s2e.magasin.beans.Commande;
import com.greta.s2e.magasin.services.ArticleService;

@Controller
@RequestMapping(value = "/stock")
public class ArticleController {
	public static final Logger LOGGER = Logger.getLogger(ArticleController.class);

	@Autowired
	private ArticleService service;

	// liste des articles en stock
	@RequestMapping(method = RequestMethod.GET)
	public String listArticles(ModelMap model) {
		LOGGER.info("initialisation de la liste des articles");
		model.addAttribute("listart", service.getList());
		return "liste_articles";
	}

//	// formulaire de création d'une commande avec un article
//	@RequestMapping(value = "/new/{id}", method = RequestMethod.GET)
//	public ModelAndView newCommande(@PathVariable int id) {
//		LOGGER.info("initialisation du formulaire de creation");
//		LOGGER.info("id : " + id);
//		Article art = service.getElementById(id);
//		return new ModelAndView("article", "cmd", art);
//	}

	// //formulaire de création
	// @RequestMapping(value = "/creer", method = RequestMethod.GET)
	// public String newUser(ModelMap model) {
	// LOGGER.info("initialisation du formulaire de creation");
	// return "new_user";
	// }
	//
	// //traitement du formulaire de création
	// @RequestMapping(value="/enregistrer", method = RequestMethod.POST)
	// public ModelAndView createUser(@ModelAttribute("user") User user){
	// LOGGER.info("création d'un User");
	// int result = service.create(new User(user.getName(), user.getPassword(),
	// user.getEmail()));
	// LOGGER.info("création ok : user " + result);
	// return new ModelAndView("redirect:"+ PAGE_LISTE);
	// }
	//
	// formulaire de modification pour passer une commande
	// @RequestMapping(value = "/new", method = RequestMethod.GET)
	// public ModelAndView majUser(@PathVariable int id) {
	// LOGGER.info("initialisation du formulaire de mise à jour");
	// LOGGER.info("id : "+ id);
	// //User user = service.getElementById(id);
	// //LOGGER.info("user : "+ user.getId());
	// String user = null;
	// return new ModelAndView("new_commande", "user", user);
	// }
	//
	// //traitement du formulaire de modification
	// @RequestMapping(value="/miseajour", method = RequestMethod.POST)
	// public ModelAndView updateUser(@ModelAttribute("user") User user){
	// LOGGER.info("mise à jour d'un User");
	// int result = service.update(new User(user.getId(), user.getName(),
	// user.getPassword(), user.getEmail()));
	// LOGGER.info("mise à jour ok : user " + result);
	// return new ModelAndView("redirect:"+ PAGE_LISTE);
	// }
	//
	// //formulaire commun de création et de modification
	// @RequestMapping(value = "/formulaire/{id}", method = RequestMethod.GET)
	// public ModelAndView modifierUser(@PathVariable int id) {
	// LOGGER.info("initialisation du formulaire");
	// LOGGER.info("id : "+ id);
	// User user = new User(0, "", "", "");
	// if (id>0){
	// user = service.getElementById(id);
	// LOGGER.info("user : "+ user.getId());
	// }
	// return new ModelAndView("form_user", "user", user);
	// }
	//
	// //traitement du formulaire commun de création et de modification
	// @RequestMapping(value="/register", method = RequestMethod.POST)
	// public ModelAndView registerUser(@ModelAttribute("user") User user){
	// LOGGER.info("creation/mise à jour d'un User "+ user.getId());
	// int result = 0;
	// if (user.getId()>0){
	// result = service.update(user);
	// } else {
	// result = service.create(user);
	// }
	// LOGGER.info("creation/mise à jour ok : user " + result);
	// return new ModelAndView("redirect:"+ PAGE_LISTE);
	// }
	//
	// //suppression d'un utilisateur
	// @RequestMapping(value = "/supprimer/{id}", method = RequestMethod.GET)
	// public ModelAndView deleteUser(@PathVariable int id) {
	// LOGGER.info("destruction d'un User");
	// int result = service.delete(new User(id, "", "", ""));
	// LOGGER.info("destruction ok : user " + result);
	// return new ModelAndView("redirect:"+ PAGE_LISTE);
	// }

}
