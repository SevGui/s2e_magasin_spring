package com.greta.s2e.magasin.globals;

public class Constants {
	
	/**
	 * ATTRIBUTES
	 */	
	/* Paramètres du cookie qui contiendra l'id de l'utilisateur connecté */
	public static final String COOKIE_S2E 				= "s2e_user";
	public static final String COOKIE_S2E_PATH 			= "/";

	/* Paramètres du session qui contiendra l'utilisateur connecté */
	public static final String SESSION_S2E 				= "s2e_user";
	
	/* Paramètres */
	public static final String PARAM_ARTICLE			= "idArticle";
	public static final String PARAM_CATALOGUE			= "idCatalogue";
	public static final String PARAM_QUANTITE			= "quantite";
	
	/* Pages */
//	public static final String REDIRECTION_LOGIN 		= "http://10.120.112.120:8080/s2e_accueil_struts/";
	public static final String REDIRECTION_LOGIN 		= "http://localhost:8080/s2e_accueil_struts/";
	public static final String RACINE 					= "/s2e_magasin_spring";
	
	/* envoi vers les XHTML */
	public static final String PAGE_DEMANDES 			= RACINE + "/demandes";
	public static final String PAGE_COMMANDES 			= RACINE + "/commandes";
	public static final String PAGE_STOCK	 			= RACINE + "/stock";
	public static final String PAGE_DECONNEXION 		= RACINE + "/deconnexion";
	
	/* Constante pour les requêtes */
	public static final String REFU_ETAT				= "REFU";
	public static final String VALI_ETAT				= "COUR";
	public static final String LIVR_ETAT				= "LIV";
	public static final String ATART_ETAT				= "ATART";
	
	/* Droits */
	public static final String DROIT_MAGASIN			= "MG";
	
	/* Table Utilisateur */
	public static final String TABLE_UTILISATEUR 					= "utilisateurs";
	public static final String TABLE_UTILISATEUR_ID 				= "utlId";
	public static final String TABLE_UTILISATEUR_IDROLE 			= "utlIdRole";
	public static final String TABLE_UTILISATEUR_NOM 				= "utlNom";
	public static final String TABLE_UTILISATEUR_PRENOM 			= "utlPrenom";
	public static final String TABLE_UTILISATEUR_IDETABLISSEMENT 	= "utlIdEtab";
	public static final String TABLE_UTILISATEUR_LOGIN 				= "utlLogin";
	public static final String TABLE_UTILISATEUR_PASSWORD 			= "utlMdp";
	
	/* Table Role */
	public static final String TABLE_ROLE 							= "roles";
	public static final String TABLE_ROLE_ID 						= "roleId";
	public static final String TABLE_ROLE_LIBELLE 					= "roleLibel";

	/* Table Etablissement */
	public static final String TABLE_ETABLISSEMENT 					= "etablissement";
	public static final String TABLE_ETABLISSEMENT_ID 				= "etabId";
	public static final String TABLE_ETABLISSEMENT_NOM 				= "etabNom";
	public static final String TABLE_ETABLISSEMENT_ADRESSE 			= "etabAdres";
	public static final String TABLE_ETABLISSEMENT_CODEPOSTAL 		= "etabCP";
	public static final String TABLE_ETABLISSEMENT_VILLE 			= "etabVille";
	public static final String TABLE_ETABLISSEMENT_BUDGET 			= "etabBudget";
	public static final String TABLE_ETABLISSEMENT_BUDGET_REST 		= "etabBudRest";

	/* Table Article */
	public static final String TABLE_ARTICLE 						= "articles";
	public static final String TABLE_ARTICLE_ID 					= "artId";
	public static final String TABLE_ARTICLE_LIBELLE 				= "artNom";
	public static final String TABLE_ARTICLE_MARQUE 				= "artMarque";
	public static final String TABLE_ARTICLE_MODELE 				= "artModele";
	public static final String TABLE_ARTICLE_EQUIVALENCE 			= "artEqui";
	public static final String TABLE_ARTICLE_PRIXESTIME 			= "artPrixMoyen";
	public static final String TABLE_ARTICLE_QUANTITETOTALE 		= "artQuanTA";

	/* Table Stock */
	public static final String TABLE_STOCK 							= "stock";
	public static final String TABLE_STOCK_ID 						= "stockId";
	public static final String TABLE_STOCK_IDARTICLE 				= "stockIdArt";
	public static final String TABLE_STOCK_QUANTITE 				= "stockQuant";
	public static final String TABLE_STOCK_SEUILMIN 				= "stockQuantMin";
	public static final String TABLE_STOCK_SEUILMAX 				= "stockQuantMax";

	/* Table Etat */
	public static final String TABLE_ETAT 							= "etats";
	public static final String TABLE_ETAT_ID 						= "etatId";
	public static final String TABLE_ETAT_LIBELLE 					= "etatLibelle";

	/* Table Demande */
	public static final String TABLE_DEMANDE 						= "demandes";
	public static final String TABLE_DEMANDE_ID 					= "dmdId";
	public static final String TABLE_DEMANDE_IDUTILISATEUR 			= "dmdIdUtil";
	public static final String TABLE_DEMANDE_DATE 					= "dmdDate";
	public static final String TABLE_DEMANDE_IDETAT 				= "dmdIdEtat";
	public static final String TABLE_DEMANDE_DELAI 					= "dmdDelai";
	public static final String TABLE_DEMANDE_IDARTICLE 				= "dmdIdArt";
	public static final String TABLE_DEMANDE_QUANTITE				= "dmdQuant";

	/* Table Fournisseur */
	public static final String TABLE_FOURNISSEUR 					= "fournisseurs";
	public static final String TABLE_FOURNISSEUR_ID 				= "fourniId";
	public static final String TABLE_FOURNISSEUR_NOM 				= "fourniNom";
	public static final String TABLE_FOURNISSEUR_ADRESSE 			= "fourniAdress";
	public static final String TABLE_FOURNISSEUR_CODEPOSTAL 		= "fourniCP";
	public static final String TABLE_FOURNISSEUR_VILLE 				= "fourniVille";

	/* Table Catalogue */
	public static final String TABLE_CATALOGUE 						= "catalogue";
	public static final String TABLE_CATALOGUE_ID 					= "catId";
	public static final String TABLE_CATALOGUE_IDFOURNISSEUR 		= "catIdFournis";
	public static final String TABLE_CATALOGUE_IDARTICLE 			= "catIdArt";
	public static final String TABLE_CATALOGUE_PRIX 				= "catPrix";
	public static final String TABLE_CATALOGUE_DELAI 				= "catDelai";

	/* Table Commande */
	public static final String TABLE_COMMANDE 						= "commandes";
	public static final String TABLE_COMMANDE_ID 					= "cmdId";
	public static final String TABLE_COMMANDE_DATE 					= "cmdDate";
	public static final String TABLE_COMMANDE_IDETAT 				= "cmdIdEtat";
	public static final String TABLE_COMMANDE_DELAI 				= "cmdDelai";
	public static final String TABLE_COMMANDE_IDUTILISATEUR 		= "cmdIdUtil";
	public static final String TABLE_COMMANDE_IDCATALOGUE 			= "cmdIdCat";
	public static final String TABLE_COMMANDE_QUANTITE 				= "cmdQuant";
	
//	//Texte boutons
//	public static final String MENU_BUDGET	 			= "Suivi Budget";
//	public static final String MENU_DEMANDE 			= "Validation Demandes";
//	public static final String MENU_DECONNEXION 		= "Déconnexion";
//	public static final String BT_MODIFIER	 			= "Modifier";
//	public static final String BT_VALIDER	 			= "Valider";
//	public static final String BT_REFUSER	 			= "Refuser";
//	

	

	public Constants() {
		// TODO Auto-generated constructor stub
	}


}
