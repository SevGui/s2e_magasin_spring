package com.greta.s2e.magasin.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greta.s2e.magasin.beans.Stock;
import com.greta.s2e.magasin.dao.StockDao;


@Service
public class StockService implements IService<Stock> {
	@Autowired
	private StockDao dao;
	
	@Override
	public List<Stock> getList() {
		return this.dao.getList();
	}

	@Override
	public int create(Stock element) {
		return dao.create(element);
	}

	@Override
	public int update(Stock element) {
		return dao.update(element);
	}

	@Override
	public int delete(Stock element) {
		return dao.delete(element);
	}

	@Override
	public Stock getElementById(int id) {
		return dao.getElementById(id);
	}

}