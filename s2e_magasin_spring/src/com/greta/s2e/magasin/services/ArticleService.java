package com.greta.s2e.magasin.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greta.s2e.magasin.beans.Article;
import com.greta.s2e.magasin.dao.ArticleDao;

@Service
public class ArticleService implements IService<Article> {
	@Autowired
	private ArticleDao dao;
	
	@Override
	public List<Article> getList() {
		return this.dao.getList();
	}

	@Override
	public int create(Article element) {
		return dao.create(element);
	}

	@Override
	public int update(Article element) {
		return dao.update(element);
	}

	@Override
	public int delete(Article element) {
		return dao.delete(element);
	}

	@Override
	public Article getElementById(int id) {
		return dao.getElementById(id);
	}

}