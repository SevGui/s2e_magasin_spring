package com.greta.s2e.magasin.services;

import java.util.List;

public interface IService<T> {
	
	public List<T> getList();
	public T getElementById(int id);
	
	public int create(T element);
	public int update(T element);
	public int delete(T element);
	
}