package com.greta.s2e.magasin.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greta.s2e.magasin.beans.Catalogue;
import com.greta.s2e.magasin.dao.CatalogueDao;

@Service
public class CatalogueService implements IService<Catalogue>{
	
	@Autowired
	private CatalogueDao dao;
	
	@Override
	public List<Catalogue> getList() {
		return this.dao.getList();
	}

	@Override
	public int create(Catalogue element) {
		return dao.create(element);
	}

	@Override
	public int update(Catalogue element) {
		return dao.update(element);
	}

	@Override
	public int delete(Catalogue element) {
		return dao.delete(element);
	}

	@Override
	public Catalogue getElementById(int id) {
		return dao.getElementById(id);
	}
	
	public List<Catalogue> getListByArticle(int idArticle) {
		return dao.getListByArticle(idArticle);
	}

}
