package com.greta.s2e.magasin.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greta.s2e.magasin.beans.Demande;
import com.greta.s2e.magasin.dao.DemandeDao;

@Service
public class DemandeService implements IService<Demande> {
	@Autowired
	private DemandeDao dao;
	
	@Override
	public List<Demande> getList() {
		return this.dao.getList();
	}

	@Override
	public int create(Demande element) {
		return dao.create(element);
	}

	@Override
	public int update(Demande element) {
		return dao.update(element);
	}

	@Override
	public int delete(Demande element) {
		return dao.delete(element);
	}

	@Override
	public Demande getElementById(int id) {
		return dao.getElementById(id);
	}

}