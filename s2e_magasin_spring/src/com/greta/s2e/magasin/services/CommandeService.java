package com.greta.s2e.magasin.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greta.s2e.magasin.beans.Commande;
import com.greta.s2e.magasin.dao.CommandeDao;

@Service
public class CommandeService implements IService<Commande> {
	@Autowired
	private CommandeDao dao;
	
	@Override
	public List<Commande> getList() {
		return this.dao.getList();
	}

	@Override
	public int create(Commande element) {
		return dao.create(element);
	}

	@Override
	public int update(Commande element) {
		return dao.update(element);
	}

	@Override
	public int delete(Commande element) {
		return dao.delete(element);
	}

	@Override
	public Commande getElementById(int id) {
		return dao.getElementById(id);
	}

}