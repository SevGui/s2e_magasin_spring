package com.greta.s2e.magasin.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greta.s2e.magasin.beans.Utilisateur;
import com.greta.s2e.magasin.dao.UtilisateurDao;



@Service
public class UtilisateurService implements IService<Utilisateur> {
	@Autowired
	private UtilisateurDao dao;
	
	@Override
	public List<Utilisateur> getList() {
		return this.dao.getList();
	}

	@Override
	public int create(Utilisateur element) {
		return dao.create(element);
	}

	@Override
	public int update(Utilisateur element) {
		return dao.update(element);
	}

	@Override
	public int delete(Utilisateur element) {
		return dao.delete(element);
	}

	@Override
	public Utilisateur getElementById(int id) {
		return dao.getElementById(id);
	}

}