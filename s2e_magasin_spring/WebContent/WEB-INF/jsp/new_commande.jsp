<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="static com.greta.s2e.magasin.globals.Constants.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>S2E - Magasin</title>
	<link href="<c:url value="/bootstrap/css/bootstrap.css" />" rel='stylesheet' />
	<link href="<c:url value="/css/styles.css" />" rel="stylesheet">
</head>
<body>
	<div class="container-fluid">
		<header class="row">
			<%@ include file="header.jsp"%>
		</header>
		
		<nav class="mainmenu">
			<div class="container">
				<%@ include file="menu.jsp"%>
			</div>
		</nav>
		
		<main class="container">
			<div class="row">
				<form method="post" action="<%=PAGE_COMMANDES%>/enregistrer" 
					class="form form-horizontal col-xs-offset-2 col-xs-8">
					<fieldset>
					
						<!-- Form Name -->
						<legend>Nouvelle Commande</legend>
					
						<!-- Text output-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="<%=PARAM_ARTICLE%>">Article</label>
							<div class="col-md-4">
								<c:out value="${article.libelle} ${article.marque} ${article.modele} - ${article.prixFormate} Euros"/>
							</div>
						</div>
						
						<!-- Select Basic -->
						<div class="form-group">
							<label class="col-md-4 control-label" for="<%= PARAM_CATALOGUE %>">Fournisseur</label>
							<div class="col-md-4">
								<select id="<%= PARAM_CATALOGUE %>" name="<%= PARAM_CATALOGUE %>"
										class="form-control">
									<option value="">- Choisissez un fournisseur -</option>
									<c:forEach items="${catalogues}" var="catalogue">
										<option value="${catalogue.id}">${catalogue.fournisseur.nom} - ${catalogue.prix} €</option>
									</c:forEach>
								</select>
							</div>
						</div>
						
						<!-- Text input-->
						<div class="form-group">
							<label class="col-md-4 control-label" for="<%=PARAM_QUANTITE%>">Quantité</label>
							<div class="col-md-4">
								<input id="<%=PARAM_QUANTITE%>" name="<%=PARAM_QUANTITE%>"
										class="form-control input-md" required=""
										type="text">
							</div>
						</div>
						
						<!-- Button -->
						<div class="form-group">
							<label class="col-md-4 control-label" for="valide"></label>
							<div class="col-md-4">
								<button id="valide" name="valide" class="btn">Valider</button>
							</div>
						</div>
						<c:if test="${ exception != null }">
							<div class="alert alert-danger">${ exception.exceptionMessage }</div>
						</c:if>
							
					</fieldset>
				</form>
			</div>
		</main>
		<footer class="container footer-wrap"> 
			<%@ include file="footer.jsp"%>
		</footer>
	</div>
</body>
</html>