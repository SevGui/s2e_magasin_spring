<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="row">
	<div class="wrap">
		<div class="col-sm-offset-1 col-sm-4">
			<div class="title">
				<h1>Testimoniala</h1>
			</div>
			<div class="content">
				<div class="testimonials">Accusantium dolo remque la udantium
					totam consequat tortor pretium sit amet volup</div>
			</div>
		</div>
		<div class="col-sm-offset-3 col-sm-3">
			<div class="title">
				<h1>Contact us</h1>
			</div>
			<div class="cotact">
				<ul>
					<li><img src="images/icon6.png" alt="" />(000) 888 888888</li>
					<li><img src="images/icon7.png" alt="" /><a href="#">info@companyname.com</a></li>
					<li><img src="images/icon8.png" alt="" />Name of site owner</li>
				</ul>
			</div>
		</div>
	</div>
</div>
