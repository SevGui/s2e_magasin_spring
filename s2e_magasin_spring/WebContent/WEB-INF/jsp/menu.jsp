<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="static com.greta.s2e.magasin.globals.Constants.*"%>
<ul class="nav nav-pills col-sm-offset-0 col-sm-5">
	<li class="active"><a href="<%=RACINE%>/stock">Stock</a></li>
	<li><a href="<%=RACINE%>/demandes">Demandes</a></li>
	<li><a href="<%=RACINE%>/commandes">Commandes</a></li>
	<li><a href="<%= PAGE_DECONNEXION %>">Déconnexion</a></li>
</ul>
<ul class="nav nav-pills col-sm-offset-3 col-sm-4">
	<li><a class="social" href="#"><img
			src="<%=RACINE%>/images/aim.png" alt="" /></a></li>
	<li><a class="social" href="#"><img
			src="<%=RACINE%>/images/facebook.png" alt="" /></a></li>
	<li><a href="#"><img src="<%=RACINE%>/images/twwtter.png"
			alt="" /></a></li>
	<li><a href="#"><img src="<%=RACINE%>/images/linkedin.png"
			alt="" /></a></li>
</ul>



