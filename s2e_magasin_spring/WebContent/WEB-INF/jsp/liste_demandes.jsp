<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="static com.greta.s2e.magasin.globals.Constants.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>S2E - Magasin</title>
	<link href='<%= RACINE %>/bootstrap/css/bootstrap.css' rel='stylesheet' />
    <link href="<%= RACINE %>/css/styles.css" rel="stylesheet">
</head>
<body>
	<div class="container-fluid">
		<header class="row">
			<%@ include file="header.jsp"%>
		</header>
		
		<nav class="mainmenu">
			<div class="container">
				<%@ include file="menu.jsp"%>
			</div>
		</nav>
		
		<main class="container">
			<div class="row">
				<div class="box col-xs-offset-3 col-xs-4 text-center">
					<div class="panel">
						<div class="title">
							<h1>Liste des demandes</h1>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Date</th>
							<th>Etablissement</th>
							<th>Article</th>
							<th>Quantité commandée</th>
							<th>Quantité stock</th>
							<th>Etat</th>
							<th></th>
						</tr>
					</thead>
					<body>
						<c:forEach var="demande" items="${liste}" varStatus="status">
							<tr>
								<td><b>${demande.date}</b></td>
								<td>${demande.demandeur.etablissement.nom}</td>
								<td>${demande.article.libelle}</td>
								<td>${demande.quantite}</td>
								<td>${demande.article.stock.quantite}</td>
								<td>${demande.etat.libelle}</td>
								<td>
								<c:choose>
									<c:when test="${demande.etat.id == 'COUR'}">
										<a class="btn" href="<%= PAGE_DEMANDES %>/livrer/${ demande.id }">Livrée</a> 
										<a class="btn" href="<%= PAGE_DEMANDES %>/new/${ demande.id }/${ demande.article.id }">Commande</a>
									</c:when>
									<c:when test="${demande.etat.id == 'ATART'}">
										<a class="btn" href="<%= PAGE_DEMANDES %>/livrer/${ demande.id }">Livrer</a> 
									</c:when>
								</c:choose>
								</td>
							</tr>
						</c:forEach>
					</body>
				</table>
			</div>
		</main>
		<footer class="container footer-wrap"> 
			<%@ include file="footer.jsp"%>
		</footer>
	</div>
</body>

</html>