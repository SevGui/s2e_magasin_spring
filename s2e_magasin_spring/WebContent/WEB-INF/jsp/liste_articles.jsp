<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="static com.greta.s2e.magasin.globals.Constants.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>S2E - Magasin</title>
	<link href='<%= RACINE %>/bootstrap/css/bootstrap.css' rel='stylesheet' />
    <link href="<%= RACINE %>/css/styles.css" rel="stylesheet">
</head>
<body>
	<div class="container-fluid">
		<header class="row">
			<%@ include file="header.jsp"%>
		</header>
		
		<nav class="mainmenu">
			<div class="container">
				<%@ include file="menu.jsp"%>
			</div>
		</nav>
		
		<main class="container">
			<div class="row">
				<div class="box col-xs-offset-3 col-xs-4 text-center">
					<div class="panel">
						<div class="title">
							<h1>Liste des articles</h1>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Libelle</th>
							<th>Marque</th>
							<th>Modele</th>
							<th>Quantité</th>
							<th>Seuil minimum</th>
							<th>Seuil maximum</th>
							<th></th>
						</tr>
					</thead>
					<body>
						<c:forEach var="article" items="${listart}" varStatus="status">
							<tr>
								<td>${article.libelle}</td>
								<td>${article.marque}</td>
								<td>${article.modele}</td>
								<td>${article.stock.quantite}</td>
								<td>${article.stock.seuilMinimum}</td>
								<td>${article.stock.seuilMaximum}</td>
								<td><a class="btn" href="<%= PAGE_COMMANDES %>/new/${article.id}">Commande</a></td>
							</tr>
						</c:forEach>
					</body>
				</table>
			</div>
		</main>
		<footer class="container footer-wrap"> 
			<%@ include file="footer.jsp"%>
		</footer>
	</div>
</body>

</html>