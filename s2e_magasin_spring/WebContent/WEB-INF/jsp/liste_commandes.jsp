<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="static com.greta.s2e.magasin.globals.Constants.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>S2E - Magasin</title>
	<link href='<%= RACINE %>/bootstrap/css/bootstrap.css' rel='stylesheet' />
    <link href="<%= RACINE %>/css/styles.css" rel="stylesheet">
</head>
<body>
	<div class="container-fluid">
		<header class="row">
			<%@ include file="header.jsp"%>
		</header>
		
		<nav class="mainmenu">
			<div class="container">
				<%@ include file="menu.jsp"%>
			</div>
		</nav>
		
		<main class="container">
			<div class="row">
				<div class="box col-xs-offset-3 col-xs-4 text-center">
					<div class="panel">
						<div class="title">
							<h1>Liste des commandes</h1>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>Date</th>
							<th>Article</th>
							<th>Quantite</th>
							<th>Fournisseur</th>
							<th>Montant</th>
							<th>Etat</th>
							<th></th>
						</tr>
					</thead>
					<body>
						<c:forEach var="commande" items="${liste}" varStatus="status">
							<tr>
								<td>${commande.date}</td>
								<td>${commande.articleCatalogue.article.marque} ${commande.articleCatalogue.article.modele} ${commande.articleCatalogue.article.libelle}</td>
								<td>${commande.quantite}</td>
								<td>${commande.articleCatalogue.fournisseur.nom}</td>
								<td>${commande.articleCatalogue.prix} €</td>
								<td>${commande.etat.libelle}</td>
								<td>
									<c:if test="${commande.etat.id == 'COUR'}">
										<a class="btn" href="<%= PAGE_COMMANDES %>/livrer/${ commande.id }">Livrée</a>
									</c:if>
								</td>
							</tr>
						</c:forEach>
					</body>
				</table>
			</div>
		
		</main>
		<footer class="container footer-wrap"> 
			<%@ include file="footer.jsp"%>
		</footer>
	</div>
</body>

</html>